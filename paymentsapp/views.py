from django.shortcuts import render

from django.http import HttpResponse

def makepayment(request):
    return render(request,"makepayment.html")

def paymentsummary(request):
    return render(request,"paymentsummary.html")    


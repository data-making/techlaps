from django.urls import path
from . import views

urlpatterns = [
    path("makepayment/", views.makepayment, name="makepayment"),
    path("makepayment/paymentsummary/", views.paymentsummary, name="paymentsummary")


]
from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.
def home(request):
    return render(request, 'home.html')

def order(request):
    return render(request, 'order.html')

# view function
'''

def function_name(request):

    return HttpResponse(<<diffent type of content>>)

# HttpResponse
== "Hello Order Page"
== {"page_name": "Order Page"}
== <html> ... </html>

'''

def detail(request):
    return render(request, 'detail.html')

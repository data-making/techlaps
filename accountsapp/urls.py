from django.urls import path, include
from . import views

urlpatterns = [
    
    path('', views.myprofile, name = "myprofile"),
    path('editprofile/', views.editprofile, name = "editprofile"),
]
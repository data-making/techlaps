from django.shortcuts import render


# Create your views here.

def myprofile(request):
    return render(request, "myprofile.html")

def editprofile(request):
    return render(request, "editprofile.html")


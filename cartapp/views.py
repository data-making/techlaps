from django.shortcuts import render

# Create your views here.

def view_cart(resquest):
    return render(resquest, "cart/view_cart.html")

def check_out(resquest):
    return render(resquest, "cart/check_out.html")
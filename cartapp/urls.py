from django.urls import path
from . import views

urlpatterns = [
    path('view_cart', views.view_cart, name="view_cart"),
    path('check_out', views.check_out, name="check_out"),
]
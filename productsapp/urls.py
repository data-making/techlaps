from django.urls import path, include

from . import views

# snake case -> create_product
# cart
# view_cart

# camel case
# cart
# viewCart
# editProduct
# createProduct

# PEP8

urlpatterns = [
    path('',views.create_product,name='create_product'),
    path('Edit_product/',views.Edit_product,name='Edit_product')
] 
from django.apps import AppConfig


class TechlapsappConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "techlapsapp"
